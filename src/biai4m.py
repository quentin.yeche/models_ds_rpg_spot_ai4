import torch
from torch import nn
from .ai4m import AI4M, get_conv_unit


class BiAI4M(AI4M):
    def __init__(self, filters=64):
        super().__init__(filters)
        self.last_decoder_unit2 = nn.Sequential(
            get_conv_unit(filters),
            get_conv_unit(filters),
            nn.LazyConv2d(1, kernel_size=1),
            nn.Sigmoid(),
        )

    def forward(self, x):
        x = self.begin_conv(x)
        # keeping the first skip connection separate since it is concatenated
        # for the last decoder unit which is not in self.decoder_units
        first_skip = x
        x = self.conv3(x)
        x = self.maxpool(x)

        skip_connections = []
        for conv_unit in self.conv_units:
            x, y = conv_unit(x)
            skip_connections.append(y)

        x = self.middle(x)

        for decoder_unit, skip_connection in zip(
            self.decoder_units, reversed(skip_connections)
        ):
            x = decoder_unit(x, skip_connection)

        x = torch.cat([x, first_skip], 1)
        x1 = self.last_decoder_unit(x)
        x2 = self.last_decoder_unit2(x)

        return {"extent": x1, "border": x2}
