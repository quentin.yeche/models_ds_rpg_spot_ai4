import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp
from .lightning_model import BaseLightingModel


class LightningModel(BaseLightingModel):

    def shared_step(self, batch, stage):
        image = batch["image"]
        extent = batch["extent"]
        border = batch["border"]
        distance = batch["distance"]
        self.assertions(image, extent)

        preds = self.forward(image)
        pred_extent, pred_border, pred_distance = (
            preds["extent"],
            preds["border"],
            preds["distance"],
        )

        loss = self.loss_fn(
            pred_extent, pred_border, pred_distance, extent, border, distance
        )

        # bsinet already contains activations for outputs
        chosen_extent = pred_extent.argmax(dim=1).unsqueeze(1)
        res_extent = self.matrix_dict(chosen_extent.long(), extent.long())

        chosen_border = pred_border.argmax(dim=1).unsqueeze(1)
        res_border = self.matrix_dict(chosen_border.long(), border.long())

        res = {
            "loss": loss,
            "extent": res_extent,
            "border": res_border,
        }
        self.outputs[stage].append(res)
        # we can either return the loss or a dictionary containing the loss value with "loss" as a key
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        dataset_iou_extent, f1_score_extent = self.compute_metrics(outputs, "extent")
        dataset_iou_border, f1_score_border = self.compute_metrics(outputs, "border")

        # aggregate lossf
        loss = torch.mean(torch.cat([x["loss"].reshape(1) for x in outputs]))

        # these metrics are meant to be shown, but only for validate
        metrics_pbar = {
            "losss": loss,
            f"{stage}_extent_iou": dataset_iou_extent,
            f"{stage}_border_iou": dataset_iou_border,
        }

        # these metrics will never be shown
        metrics_other = {
            f"{stage}_extent_f1_score": f1_score_extent,
            f"{stage}_border_f1_score": f1_score_border,
        }
        self.log_metrics(metrics_pbar, metrics_other, stage)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()
