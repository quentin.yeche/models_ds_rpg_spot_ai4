import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp

from itertools import product
from functools import partial
from torchvision.transforms.functional import hflip

from .lightning_model import BaseLightingModel

class LightningModel(BaseLightingModel):
    def __init__(self, model, loss, lr):
        super().__init__(model, loss, lr)
        self.outputs['flip'] = []
        
    def shared_step(self, batch, stage):

        image = batch["image"]
        mask = batch["mask"]
        self.assertions(image, mask)

        prob_mask = self.forward(image)

        # model already contains a sigmoid
        loss = self.loss_fn(prob_mask, mask, logits=False)

        pred_mask = (prob_mask > 0.5).float()

        res = self.matrix_dict(pred_mask.long(), mask.long())

        res['loss'] = loss

        if stage == 'test':
            res_flip = self.fliptest(batch)
            self.outputs['flip'].append(res_flip)


        self.outputs[stage].append(res)
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        dataset_iou, f1_score = self.compute_metrics(outputs)

        # aggregate lossf
        loss = torch.cat([x['loss'].reshape(1) for x in outputs]).mean()


        metrics_pbar = {
            'loss': loss,
            f"{stage}_iou": dataset_iou,
        }

        metrics_other = {
            f"{stage}_f1_score": f1_score
        }

        if stage == 'test':
            outputs = self.outputs['flip']
            
            dataset_iou, f1_score = self.compute_metrics(outputs)

            metrics_other[f"{stage}_fip_iou"] = dataset_iou
            metrics_other[f"{stage}_flip_f1_score"] = f1_score



        self.log_dict(metrics_pbar, prog_bar=True)
        self.log_dict(metrics_other, prog_bar=False)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()
    
    def fliptest(self, batch):
        def apply_d4(x):
            rotations = [partial(torch.rot90, k=i, dims=[-1,-2]) for i in range(4)]
            hflips = [lambda y:y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]
        def undo_d4(x):
            rotations = [partial(torch.rot90,k =i, dims=[-1,-2]) for i in range(4,0,-1)]
            hflips = [lambda y:y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        mask = batch["mask"]

        prob_mask = torch.stack([self.forward(image) for image in apply_d4(batch['image'])])
        prob_mask = torch.mean(prob_mask, dim=0)

        pred_mask = (prob_mask > 0.5).float()

        res = self.matrix_dict(pred_mask.long(), mask.long())

        return res
    
class LightningModel2(pl.LightningModule):

    def __init__(self, model, loss, lr, **kwargs):
        super().__init__()
        self.model = model

        self.save_hyperparameters("lr")

        self.outputs = {'train':[], 'valid':[], 'test':[]}
        self.outputs['flip'] = []

        self.loss_fn = loss

    def forward(self, image):
        # normalize image here
        mask = self.model(image)
        return mask

    def shared_step(self, batch, stage):

        image = batch["image"]

        # Shape of the image should be (batch_size, num_channels, height, width)
        # if you work with grayscale images, expand channels dim to have [batch_size, 1, height, width]
        assert image.ndim == 4

        # Check that image dimensions are divisible by 32,
        # encoder and decoder connected by `skip connections` and usually encoder have 5 stages of
        # downsampling by factor 2 (2 ^ 5 = 32); e.g. if we have image with shape 65x65 we will have
        # following shapes of features in encoder and decoder: 84, 42, 21, 10, 5 -> 5, 10, 20, 40, 80
        # and we will get an error trying to concat these features
        h, w = image.shape[2:]
        assert h % 32 == 0 and w % 32 == 0

        mask = batch["mask"]

        # Shape of the mask should be [batch_size, num_classes, height, width]
        # for binary segmentation num_classes = 1
        assert mask.ndim == 4

        # Check that mask values in between 0 and 1, NOT 0 and 255 for binary segmentation
        assert mask.max() <= 1.0 and mask.min() >= 0

        logits_mask = self.forward(image)

        # Predicted mask contains logits, and loss_fn param `from_logits` is set to True
        loss = self.loss_fn(logits_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = (prob_mask > 0.5).float()

        # We will compute IoU metric by two ways
        #   1. dataset-wise
        #   2. image-wise
        # but for now we just compute true positive, false positive, false negative and
        # true negative 'pixels' for each image and class
        # these values will be aggregated in the end of an epoch
        tp, fp, fn, tn = smp.metrics.get_stats(pred_mask.long(), mask.long(), mode="binary")



        res = {
            "loss": loss,
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "tn": tn,
        }

        if stage == 'test':
            res_flip = self.fliptest(batch)
            self.outputs['flip'].append(res_flip)


        self.outputs[stage].append(res)
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        # aggregate step metics
        tp = torch.cat([x["tp"] for x in outputs])
        fp = torch.cat([x["fp"] for x in outputs])
        fn = torch.cat([x["fn"] for x in outputs])
        tn = torch.cat([x["tn"] for x in outputs])

        # aggregate lossf
        loss = torch.cat([x['loss'].reshape(1) for x in outputs])

        # per image IoU means that we first calculate IoU score for each image
        # and then compute mean over these scores
        #per_image_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro-imagewise")

        # dataset IoU means that we aggregate intersection and union over whole dataset
        # and then compute IoU score. The difference between dataset_iou and per_image_iou scores
        # in this particular case will not be much, however for dataset
        # with "empty" images (images without target class) a large gap could be observed.
        # Empty images influence a lot on per_image_iou and much less on dataset_iou.
        dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
    
        f1_score = tp.sum()/(tp.sum() + 0.5*(fp.sum() + fn.sum()))

        metrics = {
            #f"{stage}_per_image_iou": per_image_iou,
            #f"{stage}_dataset_iou": dataset_iou,
            f"{stage}_iou": dataset_iou,
            f"{stage}_f1_score": f1_score
        }

        if stage == 'test':
            outputs = self.outputs['flip']
            # aggregate step metics
            tp = torch.cat([x["tp"] for x in outputs])
            fp = torch.cat([x["fp"] for x in outputs])
            fn = torch.cat([x["fn"] for x in outputs])
            tn = torch.cat([x["tn"] for x in outputs])

            # aggregate lossf
            loss = torch.cat([x['loss'].reshape(1) for x in outputs])

            # per image IoU means that we first calculate IoU score for each image
            # and then compute mean over these scores
            #per_image_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro-imagewise")

            # dataset IoU means that we aggregate intersection and union over whole dataset
            # and then compute IoU score. The difference between dataset_iou and per_image_iou scores
            # in this particular case will not be much, however for dataset
            # with "empty" images (images without target class) a large gap could be observed.
            # Empty images influence a lot on per_image_iou and much less on dataset_iou.
            dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
        
            f1_score = tp.sum()/(tp.sum() + 0.5*(fp.sum() + fn.sum()))

            metrics[f"{stage}_fip_iou"] = dataset_iou
            metrics[f"{stage}_flip_f1_score"] = f1_score



        self.log_dict(metrics, prog_bar=True)

        self.log_dict({'loss':torch.mean(loss)}, prog_bar=True)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()

    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train")

    def on_train_epoch_end(self):
        return self.shared_epoch_end("train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid")

    def on_validation_epoch_end(self):
        return self.shared_epoch_end("valid")

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test")

    def on_test_epoch_end(self):
        return self.shared_epoch_end("test")
    
    def fliptest(self, batch):
        def apply_d4(x):
            rotations = [partial(torch.rot90, k=i, dims=[-1,-2]) for i in range(4)]
            hflips = [lambda y:y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]
        def undo_d4(x):
            rotations = [partial(torch.rot90,k =i, dims=[-1,-2]) for i in range(4,0,-1)]
            hflips = [lambda y:y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        mask = batch["mask"]

        logits_mask = torch.stack([self.forward(image) for image in apply_d4(batch['image'])])
        logits_mask = torch.mean(logits_mask, dim=0)

        loss = self.loss_fn(logits_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = (prob_mask > 0.5).float()

        # We will compute IoU metric by two ways
        #   1. dataset-wise
        #   2. image-wise
        # but for now we just compute true positive, false positive, false negative and
        # true negative 'pixels' for each image and class
        # these values will be aggregated in the end of an epoch
        tp, fp, fn, tn = smp.metrics.get_stats(pred_mask.long(), mask.long(), mode="binary")

        
        res = {
            "loss": loss,
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "tn": tn,
        }
        return res

