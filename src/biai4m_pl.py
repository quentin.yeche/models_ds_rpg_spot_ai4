import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp

from itertools import product
from functools import partial
from torchvision.transforms.functional import hflip

from .lightning_model import BaseLightingModel


class LightningModel(BaseLightingModel):
    def __init__(self, model, loss, lr):
        super().__init__(model, loss, lr)
        self.outputs["flip_extent"] = []
        self.outputs["flip_border"] = []

    def shared_step(self, batch, stage):

        image = batch["image"]
        extent = batch["extent"]
        border = batch["border"]
        self.assertions(image, extent)

        preds = self.forward(image)
        logits_extent, logits_border = preds["extent"], preds["border"]

        # Predicted mask contains logits, and loss_fn param `from_logits` is set to True
        loss = self.loss_fn(logits_extent, extent) + self.loss_fn(logits_border, border)

        pred_extent = (logits_extent.sigmoid() > 0.5).float()
        res_extent = self.matrix_dict(pred_extent.long(), extent.long())

        pred_border = (logits_border.sigmoid() > 0.5).float()
        res_border = self.matrix_dict(pred_border.long(), border.long())

        res = {
            "loss": loss,
            "extent": res_extent,
            "border": res_border,
        }

        if stage == "test":
            res_flip_extent, res_flip_border = self.fliptest(batch)
            self.outputs["flip_extent"].append(res_flip_extent)
            self.outputs["flip_border"].append(res_flip_border)

        self.outputs[stage].append(res)
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        dataset_iou_extent, f1_score_extent = self.compute_metrics(outputs, "extent")
        dataset_iou_border, f1_score_border = self.compute_metrics(outputs, "border")

        # aggregate lossf
        loss = torch.mean(torch.cat([x["loss"].reshape(1) for x in outputs]))

        # these metrics are meant to be shown, but only for validate
        metrics_pbar = {
            "loss": loss,
            f"{stage}_extent_iou": dataset_iou_extent,
            f"{stage}_border_iou": dataset_iou_border,
        }

        # these metrics will never be shown
        metrics_other = {
            f"{stage}_extent_f1_score": f1_score_extent,
            f"{stage}_border_f1_score": f1_score_border,
        }
        self.log_dict(metrics_pbar, prog_bar=True)
        self.log_dict(metrics_other, prog_bar=False)

        if stage == "test":
            outputs_extent = self.outputs["flip_extent"]
            extent_dataset_iou, extent_f1_score = self.compute_metrics(outputs_extent)

            outputs_border = self.outputs["flip_border"]
            border_dataset_iou, border_f1_score = self.compute_metrics(outputs_border)

            flip_res = {
                "flip_extent_iou": extent_dataset_iou,
                "flip_extent_f1": extent_f1_score,
                "flip_border_iou": border_dataset_iou,
                "flip_border_f1": border_f1_score,
            }
            self.log_dict(flip_res)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()

    def fliptest(self, batch):
        def apply_d4(x):
            rotations = [partial(torch.rot90, k=i, dims=[-1, -2]) for i in range(4)]
            hflips = [lambda y: y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        def undo_d4(x):
            rotations = [
                partial(torch.rot90, k=i, dims=[-1, -2]) for i in range(4, 0, -1)
            ]
            hflips = [lambda y: y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        extent = batch["extent"]
        border = batch["border"]

        logits = [self.forward(image) for image in apply_d4(batch["image"])]

        logits_extent = torch.stack([x["extent"] for x in logits])
        logits_extent = torch.mean(logits_extent, dim=0)
        pred_extent = (logits_extent.sigmoid() > 0.5).float()
        res_extent = self.matrix_dict(pred_extent.long(), extent.long())

        logits_border = torch.stack([x["border"] for x in logits])
        logits_border = torch.mean(logits_border, dim=0)
        pred_border = (logits_border.sigmoid() > 0.5).float()
        res_border = self.matrix_dict(pred_border.long(), border.long())

        return res_extent, res_border


class LightningModel2(pl.LightningModule):

    def __init__(self, model, loss, lr, **kwargs):
        super().__init__()
        self.model = model

        self.save_hyperparameters("lr")

        self.outputs = {"train": [], "valid": [], "test": []}
        self.outputs["flip"] = []

        self.loss_fn = loss

    def forward(self, image):
        # normalize image here
        mask = self.model(image)
        return mask

    def shared_step(self, batch, stage):

        image = batch["image"]

        # Shape of the image should be (batch_size, num_channels, height, width)
        # if you work with grayscale images, expand channels dim to have [batch_size, 1, height, width]
        assert image.ndim == 4

        # Check that image dimensions are divisible by 32,
        # encoder and decoder connected by `skip connections` and usually encoder have 5 stages of
        # downsampling by factor 2 (2 ^ 5 = 32); e.g. if we have image with shape 65x65 we will have
        # following shapes of features in encoder and decoder: 84, 42, 21, 10, 5 -> 5, 10, 20, 40, 80
        # and we will get an error trying to concat these features
        h, w = image.shape[2:]
        assert h % 32 == 0 and w % 32 == 0

        mask = batch["mask"]

        # Shape of the mask should be [batch_size, num_classes, height, width]
        # for binary segmentation num_classes = 1
        assert mask.ndim == 4

        # Check that mask values in between 0 and 1, NOT 0 and 255 for binary segmentation
        assert mask.max() <= 1.0 and mask.min() >= 0

        logits_mask = self.forward(image)

        # Predicted mask contains logits, and loss_fn param `from_logits` is set to True
        loss = self.loss_fn(logits_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = (prob_mask > 0.5).float()

        # We will compute IoU metric by two ways
        #   1. dataset-wise
        #   2. image-wise
        # but for now we just compute true positive, false positive, false negative and
        # true negative 'pixels' for each image and class
        # these values will be aggregated in the end of an epoch
        tp, fp, fn, tn = smp.metrics.get_stats(
            pred_mask.long(), mask.long(), mode="binary"
        )

        res = {
            "loss": loss,
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "tn": tn,
        }

        if stage == "test":
            res_flip = self.fliptest(batch)
            self.outputs["flip"].append(res_flip)

        self.outputs[stage].append(res)
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        # aggregate step metics
        tp = torch.cat([x["tp"] for x in outputs])
        fp = torch.cat([x["fp"] for x in outputs])
        fn = torch.cat([x["fn"] for x in outputs])
        tn = torch.cat([x["tn"] for x in outputs])

        # aggregate lossf
        loss = torch.cat([x["loss"].reshape(1) for x in outputs])

        # per image IoU means that we first calculate IoU score for each image
        # and then compute mean over these scores
        # per_image_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro-imagewise")

        # dataset IoU means that we aggregate intersection and union over whole dataset
        # and then compute IoU score. The difference between dataset_iou and per_image_iou scores
        # in this particular case will not be much, however for dataset
        # with "empty" images (images without target class) a large gap could be observed.
        # Empty images influence a lot on per_image_iou and much less on dataset_iou.
        dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")

        f1_score = tp.sum() / (tp.sum() + 0.5 * (fp.sum() + fn.sum()))

        metrics = {
            # f"{stage}_per_image_iou": per_image_iou,
            # f"{stage}_dataset_iou": dataset_iou,
            f"{stage}_iou": dataset_iou,
            f"{stage}_f1_score": f1_score,
        }

        if stage == "test":
            outputs = self.outputs["flip"]
            # aggregate step metics
            tp = torch.cat([x["tp"] for x in outputs])
            fp = torch.cat([x["fp"] for x in outputs])
            fn = torch.cat([x["fn"] for x in outputs])
            tn = torch.cat([x["tn"] for x in outputs])

            # aggregate lossf
            loss = torch.cat([x["loss"].reshape(1) for x in outputs])

            # per image IoU means that we first calculate IoU score for each image
            # and then compute mean over these scores
            # per_image_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro-imagewise")

            # dataset IoU means that we aggregate intersection and union over whole dataset
            # and then compute IoU score. The difference between dataset_iou and per_image_iou scores
            # in this particular case will not be much, however for dataset
            # with "empty" images (images without target class) a large gap could be observed.
            # Empty images influence a lot on per_image_iou and much less on dataset_iou.
            dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")

            f1_score = tp.sum() / (tp.sum() + 0.5 * (fp.sum() + fn.sum()))

            metrics[f"{stage}_fip_iou"] = dataset_iou
            metrics[f"{stage}_flip_f1_score"] = f1_score

        self.log_dict(metrics, prog_bar=True)

        self.log_dict({"loss": torch.mean(loss)}, prog_bar=True)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()

    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train")

    def on_train_epoch_end(self):
        return self.shared_epoch_end("train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid")

    def on_validation_epoch_end(self):
        return self.shared_epoch_end("valid")

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test")

    def on_test_epoch_end(self):
        return self.shared_epoch_end("test")

    def fliptest(self, batch):
        def apply_d4(x):
            rotations = [partial(torch.rot90, k=i, dims=[-1, -2]) for i in range(4)]
            hflips = [lambda y: y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        def undo_d4(x):
            rotations = [
                partial(torch.rot90, k=i, dims=[-1, -2]) for i in range(4, 0, -1)
            ]
            hflips = [lambda y: y, hflip]
            return [rotation(flip(x)) for rotation, flip in product(rotations, hflips)]

        mask = batch["mask"]

        logits_mask = torch.stack(
            [self.forward(image) for image in apply_d4(batch["image"])]
        )
        logits_mask = torch.mean(logits_mask, dim=0)

        loss = self.loss_fn(logits_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = (prob_mask > 0.5).float()

        # We will compute IoU metric by two ways
        #   1. dataset-wise
        #   2. image-wise
        # but for now we just compute true positive, false positive, false negative and
        # true negative 'pixels' for each image and class
        # these values will be aggregated in the end of an epoch
        tp, fp, fn, tn = smp.metrics.get_stats(
            pred_mask.long(), mask.long(), mode="binary"
        )

        res = {
            "loss": loss,
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "tn": tn,
        }
        return res
