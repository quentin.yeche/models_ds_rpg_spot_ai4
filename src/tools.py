from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

from itertools import cycle
import numpy as np

import torch
from torchvision.transforms import functional as TF

import wandb
from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl

from pytorch_lightning.callbacks.progress.tqdm_progress import TQDMProgressBar

from pathlib import Path
import geopandas as gpd

import json


def compute_metrics(outputs):
    dataset_stats = collate_stats(outputs)
    iou = iou_score(**dataset_stats)
    f1 = f1_score(**dataset_stats)
    return iou, f1


def print_grid(pictures, labels=None, fileName=None):
    number_samples = len(pictures)
    pictures_per_sample = len(pictures[0])
    fig = plt.figure(figsize=((number_samples * 4, pictures_per_sample * 4)))
    grid = ImageGrid(
        fig,
        111,
        nrows_ncols=(pictures_per_sample, number_samples),
        axes_pad=0.2,
        direction="column",
        label_mode="L",
    )
    flat_pictures = [picture for row in pictures for picture in row]
    if labels:
        labels = cycle(labels)
    for ax, im in zip(grid, flat_pictures):
        ax.imshow(im.astype(np.float32))
        if labels:
            ax.set_ylabel(next(labels), rotation=0, fontsize=12, labelpad=20)
    if fileName is not None:
        plt.savefig(fileName)
    plt.show()


def beautify_plottable(image):
    image = image[:3].transpose((1, 2, 0))
    im_min, im_max = image.min(axis=(0, 1)), image.max(axis=(0, 1))
    image = (image - im_min) / (im_max - im_min)
    return image


def show_samples2(dataloader, labels, nb_samples):
    batch = next(iter(dataloader))
    samples = list(zip((batch["image"].numpy()), batch["mask"].numpy()))[:nb_samples]
    samples = [
        [im.squeeze() if im.shape[0] == 1 else beautify_plottable(im) for im in sample]
        for sample in samples
    ]
    print_grid(samples, labels)


def show_samples(dataloader, labels, nb_samples):
    batch = next(iter(dataloader))
    samples = list(zip(*(batch[key].numpy() for key in labels)))[:nb_samples]
    samples = [
        [im[-1] if im.shape[0] < 3 else beautify_plottable(im) for im in sample]
        for sample in samples
    ]
    print_grid(samples, labels)


class Rot90Flips(torch.nn.Module):
    def forward(self, *args):
        flip_choices = torch.randint(0, 2, (2,))
        if flip_choices[0]:
            args = [TF.hflip(arg) for arg in args]
        if flip_choices[1]:
            args = [TF.vflip(arg) for arg in args]
        num = torch.randint(0, 4, (1,)).item()
        args = [torch.rot90(arg, k=num, dims=[-1, -2]) for arg in args]
        return args


def training_loop(
    model,
    train_dataloader,
    valid_dataloader,
    project_name,
    max_epochs,
    hyperparameters,
    callbacks=None,
    accelerator=None,
):
    wandb.finish()
    if accelerator is None:
        accelerator = "auto"
    wandb_logger = WandbLogger(
        log_model=True, project=project_name, save_dir="./checkpoints/"
    )  # log_model=True waits until the end to save model, 'all' will save all checkpoints
    wandb_logger.log_hyperparams(hyperparameters)

    if callbacks is None:
        callbacks = []

    model.train()

    trainer = pl.Trainer(
        accelerator=accelerator,
        devices=1,
        max_epochs=max_epochs,
        logger=wandb_logger,
        callbacks=callbacks,
    )

    torch.set_float32_matmul_precision("high")

    trainer.fit(
        model,
        train_dataloaders=train_dataloader,
        val_dataloaders=valid_dataloader,
    )

    return model, trainer, wandb_logger


def validate_test(model, trainer, valid_dataloader, test_dataloader):
    # run validation and test datasets
    # no need to use cktp_path='best' since we should normally have already loaded
    # the best checkpoint manually
    valid_metrics = trainer.validate(model, dataloaders=valid_dataloader, verbose=False)

    test_metrics = trainer.test(model, dataloaders=test_dataloader, verbose=False)
    return valid_metrics, test_metrics


def move_dic(dic, device):
    new_dic = {
        key: (value.to(device) if isinstance(value, torch.Tensor) else value)
        for (key, value) in dic.items()
    }
    return new_dic


def move_dic_list(dic_list, device):
    return [move_dic(x, device) for x in dic_list]


class DoNothing:
    def __call__(self, x):
        return x

    def __getstate__(self) -> object:
        return self.__dict__

    def __setstate__(self, d):
        self.__dict__ = d


def get_stats(pred, target):
    if not (pred.shape == target.shape):
        print(
            f"Dimensions for pred are {pred.shape}, which does not match with target dimensions{target.shape}"
        )
    if len(pred.shape) != 4:
        raise ValueError(
            f"Target and pred should have (N,C,H,W) dimensions, but instead have shape {pred.shape}"
        )
    batch_size, num_classes, *dims = target.shape
    pred = pred.reshape(batch_size, num_classes, -1)
    target = target.reshape(batch_size, num_classes, -1)

    tp = (pred * target).sum((-1, -2))
    fp = pred.sum((-1, -2)) - tp
    fn = target.sum((-1, -2)) - tp
    tn = torch.prod(torch.tensor(dims)) - (tp + fp + fn)

    return {"tp": tp, "fp": fp, "fn": fn, "tn": tn}


def reduce(tp, fp, fn, tn, reduction):
    if reduction == "micro":
        tp = tp.sum()
        fp = fp.sum()
        fn = fn.sum()
        tn = tn.sum()
    elif reduction == "none" or reduction is None:
        pass
    else:
        raise ValueError(f"Invalid argument for reduction : {reduction}")
    return tp, fp, fn, tn


def iou_score(tp, fp, fn, tn, reduction="micro"):
    tp, fp, fn, tn = reduce(tp, fp, fn, tn, reduction)
    return (tp + 1e-12) / (tp + fp + fn + 1e-12)


def f1_score(tp, fp, fn, tn, reduction="micro"):
    tp, fp, fn, tn = reduce(tp, fp, fn, tn, reduction)
    return (2 * tp + 1e-12) / (2 * tp + fp + fn + 1e-12)


def collate_stats(stats):
    tp = torch.cat([stat["tp"] for stat in stats]).sum()
    fp = torch.cat([stat["fp"] for stat in stats]).sum()
    fn = torch.cat([stat["fn"] for stat in stats]).sum()
    tn = torch.cat([stat["tn"] for stat in stats]).sum()
    return {"tp": tp, "fp": fp, "fn": fn, "tn": tn}


class LitProgressBar(TQDMProgressBar):
    BAR_FORMAT = "{l_bar}{bar}| {n_fmt}/{total_fmt} [{elapsed}<{remaining}{postfix}]"

    def get_metrics(self, trainer, model):
        items = super().get_metrics(trainer, model)
        items.pop("v_num", None)
        return items


def get_rpg_patches(root, split_num=0, patches_prefix=None, sample=None):
    splits = ["train", "validate", "test"]
    image_types = ["image", "mask", "distance"]
    if patches_prefix is None:
        patches_prefix = "elong_10"

        split_num = 0
        patches_prefix = "elong_10"
        patches_dir = Path(root, patches_prefix)
        patches = gpd.read_file(Path(patches_dir, f"splits/split_{split_num}.geojson"))
        patches.loc[patches["split"] == "val", "split"] = "validate"

        with open(
            Path(patches_dir, f"splits/split_{split_num}_min_max.json"), "r"
        ) as f:
            min_max = json.load(f)

        dirss = ["XS", "RPG", "distances"]

        if sample is not None:
            patches = patches.sample(frac=1).head(sample)

        paths = {
            image_type: {
                split: [
                    Path(patches_dir, dirs, patch_name)
                    for patch_name in patches[patches["split"] == split]["patch_name"]
                ]
                for split in splits
            }
            for (image_type, dirs) in zip(image_types, dirss)
        }
        return {"paths": paths, "min_max": min_max}


def get_ai4s_patches(root, sample=None):
    patches_dir = Path(root)
    patches = gpd.read_file(Path(patches_dir, "patches.json"))
    with open(Path(patches_dir, "min_max.json"), "r") as f:
        min_max = json.load(f)

    splits = ["train", "validate", "test"]
    image_types = ["image", "mask", "distance"]

    if sample is not None:
        patches = patches.sample(frac=1).head(sample)

    paths = {
        image_type: {
            split: [
                Path(patches_dir, split, image_type, patch_name)
                for patch_name in patches[patches["split"] == split]["patch_name"]
            ]
            for split in splits
        }
        for image_type in image_types
    }

    return {"paths": paths, "min_max": min_max}


def get_AdamWOneCycle(parent, max_lr=0.01):
    from torch.optim.lr_scheduler import OneCycleLR

    class AdamWOneCycle(parent):
        def configure_optimizers(self):
            optimizer = torch.optim.AdamW(
                self.parameters(),
                lr=self.hparams.lr,
            )
            stepping_batches = self.trainer.estimated_stepping_batches
            scheduler_dict = {
                "scheduler": OneCycleLR(
                    optimizer, max_lr, total_steps=stepping_batches
                ),
                "interval": "step",
            }
            return {"optimizer": optimizer, "lr_scheduler": scheduler_dict}

    return AdamWOneCycle


def get_AdamWMultiStep(parent, milestones, gamma=0.3):
    from torch.optim.lr_scheduler import MultiStepLR

    class AdamWMultiStep(parent):
        def configure_optimizers(self):
            optimizer = torch.optim.AdamW(
                self.parameters(),
                lr=self.hparams.lr,
            )
            scheduler_dict = {
                "scheduler": MultiStepLR(optimizer, milestones=milestones, gamma=gamma),
                "interval": "step",
            }
            return {"optimizer": optimizer, "lr_scheduler": scheduler_dict}

    return AdamWMultiStep
