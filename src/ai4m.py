import torch
from torch import nn


def get_conv_unit(filters):
    return nn.Sequential(
        nn.LazyBatchNorm2d(momentum=0.01, affine=False),
        nn.LazyConv2d(filters, 3, padding=1),
        nn.ReLU(),
    )


def get_upconv_unit(filters):
    return nn.Sequential(
        nn.LazyBatchNorm2d(momentum=0.01, affine=False),
        nn.LazyConvTranspose2d(
            filters, kernel_size=3, stride=2, padding=1, output_padding=1
        ),
        nn.ReLU(),
    )


class EncoderUnit(nn.Module):
    def __init__(self, filters):
        super().__init__()
        self.conv1 = get_conv_unit(filters)
        self.conv2 = get_conv_unit(filters)
        self.conv3 = get_conv_unit(filters)
        self.maxpool = nn.MaxPool2d(kernel_size=2)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        y = x
        x = self.conv3(x)
        x = self.maxpool(x)
        return x, y


class DecoderUnit(nn.Module):
    def __init__(self, filters):
        super().__init__()
        self.conv1 = get_conv_unit(filters)
        self.conv2 = get_conv_unit(filters)
        self.conv3 = get_upconv_unit(filters)

    def forward(self, x, y):
        x = torch.cat([x, y], 1)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        return x


class AI4M(nn.Module):
    def __init__(self, filters=64):
        super().__init__()

        self.begin_conv = nn.Sequential(
            nn.LazyConv2d(filters, kernel_size=3, padding=1),
            nn.ReLU(),
            get_conv_unit(filters),
        )

        self.conv3 = get_conv_unit(filters)
        self.maxpool = nn.MaxPool2d(kernel_size=2)

        self.conv_units = nn.ModuleList([EncoderUnit(filters) for _ in range(4)])

        self.middle = nn.Sequential(
            get_conv_unit(filters),
            get_conv_unit(filters),
            get_upconv_unit(filters),
        )

        self.decoder_units = nn.ModuleList([DecoderUnit(filters) for _ in range(4)])

        self.last_decoder_unit = nn.Sequential(
            get_conv_unit(filters),
            get_conv_unit(filters),
            nn.LazyConv2d(1, kernel_size=1),
            nn.Sigmoid(),
        )

    def forward(self, x):
        x = self.begin_conv(x)
        # keeping the first skip connection separate since it is concatenated
        # for the last decoder unit which is not in self.decoder_units
        first_skip = x
        x = self.conv3(x)
        x = self.maxpool(x)

        skip_connections = []
        for conv_unit in self.conv_units:
            x, y = conv_unit(x)
            skip_connections.append(y)

        x = self.middle(x)

        for decoder_unit, skip_connection in zip(
            self.decoder_units, reversed(skip_connections)
        ):
            x = decoder_unit(x, skip_connection)

        x = torch.cat([x, first_skip], 1)
        x = self.last_decoder_unit(x)

        return x
