from torch.utils.data import Dataset as BaseDataset
import torch
import rasterio as rio
from pathlib import Path
import numpy as np
from skimage.color import rgb2hsv
import warnings
from multiprocessing import Pool
from tqdm.auto import tqdm
from itertools import repeat
from typing import Sequence, Callable


class Dataset(BaseDataset):
    """Dataset. Read images, apply augmentation and preprocessing transformations.
    Single mask

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files
        mask_band (int): band number (0-indexed) to read from the mask file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths,
        mask_paths,
        mask_band,
        augmentation=None,
        normalize=None,
        mask_type=None,
    ):

        if mask_type is None:
            mask_type = "int16"

        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths
        self.mask_paths = mask_paths

        self.mask_type = mask_type

        self.mask_band = mask_band
        self.augmentation = augmentation
        #self.normalize = {key: np.array(val) for key, val in normalize.items()}
        self.normalize = {key: torch.Tensor(val) for key, val in normalize.items()}

    def __getitem__(self, i):
        id = self.ids[i]
        image = rio.open(self.image_paths[i]).read()
        image = torch.from_numpy(image.astype(np.int32))

        mask = rio.open(self.mask_paths[i], dtype=self.mask_type).read()[self.mask_band]
        mask = torch.from_numpy(mask.astype(self.mask_type))

        if self.normalize:
            image = image.permute(1, 2, 0)
            image = (image - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            image = image.permute(2, 0, 1).float()

        # apply augmentations
        if self.augmentation:
            image, mask = self.augmentation(image, mask)

        mask = mask.unsqueeze(0)
        # TODO remove [None,...] and add an usqueeze() to self.masks
        return {"image": image, "mask": mask, "id": id}

    def __len__(self):
        return len(self.ids)


class DatasetMulti(BaseDataset):
    """Dataset. Read images, apply augmentation and preprocessing transformations.
    Multi mask

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files
        mask_band (int): band number (0-indexed) to read from the mask file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths: Sequence[Sequence[Path | str]],
        mask_paths: Sequence[Sequence[Path | str]] | Sequence[Path | str],
        mask_names: Sequence[str],
        mask_bands: Sequence[int],
        mask_type: str,
        augmentation: Callable = None,
        normalize: dict[str, int] = None,
    ):

        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths

        self.mask_names = mask_names

        self.single_mask_path = not isinstance(mask_paths[0], Sequence)

        self.mask_paths = mask_paths
        self.mask_bands = mask_bands

        self.mask_type = mask_type

        self.augmentation = augmentation
        self.normalize = {key: np.array(val) for key, val in normalize.items()}

    def __getitem__(self, i):
        id = self.ids[i]
        image = rio.open(self.image_paths[i]).read()
        image = torch.from_numpy(image.astype(np.int32))

        if self.single_mask_path:
            masks = rio.open(self.mask_paths[i]).read()[self.mask_bands]
        else:
            masks = [
                rio.open(mask_path[i]).read()[mask_band]
                for mask_band, mask_path in zip(self.mask_bands, self.mask_paths)
            ]
        masks = [
            torch.from_numpy(mask.astype(self.mask_type)).unsqueeze(0) for mask in masks
        ]

        if self.normalize:
            image = image.permute(1, 2, 0)
            image = (image - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            image = image.permute(2, 0, 1).float()

        # apply augmentations
        if self.augmentation:
            image, *masks = self.augmentation(image, *masks)

        return {"image": image, "id": id} | {
            mask_name: mask for mask_name, mask in zip(self.mask_names, masks)
        }

    def __len__(self):
        return len(self.ids)


def getMobileSameDatasetClass(parent):
    class MobileSamDataset(parent):
        def __getitem__(self, i):
            sample = super().__getitem__(i)
            image = torch.nn.functional.interpolate(
                sample["image"][:3].unsqueeze(0),
                1024,
                mode="bilinear",
                align_corners=False,
                antialias=True,
            ).squeeze()
            sample = sample | {
                "image": image,
                "original_size": (256, 256),
            }
            return sample

    return MobileSamDataset


MobileSamDataset = getMobileSameDatasetClass(Dataset)
MobileSamDatasetMulti = getMobileSameDatasetClass(DatasetMulti)



class TriDataset(BaseDataset):
    """TriDataset. Read images, apply augmentation and preprocessing transformations.
    Three masks: extent, border, distance

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files (extent and border)
        distance_paths (str or Path list): paths to the distance files
        extent_band (int): band number (0-indexed) to read the extent from the mask file
        contour_band (int): band number (0-indexed) to read the contour from the mask file
        distance_band (int): band number (0-indexed) to read the distance from the distance file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths,
        mask_paths,
        distance_paths,
        extent_band,
        border_band,
        distance_band,
        augmentation=None,
        normalize=None,
    ):
        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths
        self.mask_paths = mask_paths
        self.distance_paths = distance_paths

        self.extent_band = extent_band
        self.border_band = border_band
        self.distance_band = distance_band

        self.augmentation = augmentation
        self.normalize = {key: np.array(val) for key, val in normalize.items()}

        self.images = torch.from_numpy(
            np.array([rio.open(fp).read() for fp in self.image_paths]).astype(np.int32)
        )
        extents, borders = [], []
        for fp in self.mask_paths:
            with rio.open(fp) as ds:
                ar = ds.read()
                extents.append(ar[self.extent_band])
                borders.append(ar[self.border_band])
        self.extents = torch.from_numpy(np.array(extents))

        # should help with not running out of memory
        del extents
        self.contours = torch.from_numpy(np.array(borders))
        del borders
        names_distances = [path.name for path in self.distance_paths]
        names_images = [path.name for path in self.image_paths]
        assert names_distances == names_images
        self.distances = torch.from_numpy(
            np.array(
                [rio.open(fp).read()[self.distance_band] for fp in self.distance_paths]
            )
        )

        if self.normalize:
            images = self.images.permute(0, 2, 3, 1)
            images = (images - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            self.images = images.permute(0, 3, 1, 2).float()

    def __getitem__(self, i):
        # read data
        image = self.images[i]
        extent = self.extents[i].unsqueeze(0)
        border = self.contours[i].unsqueeze(0)
        distance = self.distances[i].unsqueeze(0)

        id = self.ids[i]

        # apply augmentations
        if self.augmentation:
            image, extent, border, distance = self.augmentation(
                image, extent, border, distance
            )

        return {
            "image": image,
            "extent": extent.long(),
            "border": border.long(),
            "distance": distance,
            "id": id,
        }

    def __len__(self):
        return len(self.ids)


class QuadDataset(TriDataset):
    """Small modification of TriDataset which:
    - produces an HSV version of the image on the fly
    - converts extents to one-hot encoding
    - adds a second (1-p) class to contour and distance
    """

    def __getitem__(self, i):
        dic = super().__getitem__(i)
        image = dic["image"]
        # rgb2hsv tends to produce warnings as part of its normal operation
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            hsv = rgb2hsv(image[:3].numpy(), channel_axis=0)
            hsv = torch.from_numpy(hsv)

        extent = dic["extent"].squeeze()
        extent = torch.eye(2)[extent.type(torch.int32)].permute(2, 0, 1)

        border = dic["border"]
        contour_bis = 1 - border
        border = torch.cat([contour_bis, border], 0)

        distance = dic["distance"]
        distance_bis = 1 - distance
        distance = torch.cat([distance_bis, distance], 0)

        return {
            "image": image.type(torch.float32),
            "extent": extent.type(torch.float32),
            "border": border.type(torch.float32),
            "distance": distance.type(torch.float32),
            "hsv": hsv,
            "id": dic["id"],
        }
