import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp

from .lightning_model import BaseLightingModel

class LightningModel(BaseLightingModel):
    def shared_step(self, batch, stage):
        image = batch["image"]
        extent = batch["extent"]
        border = batch['border']
        self.assertions(image, extent)

        preds = self.forward(image)

        losses = [self.loss_fn(preds[x], batch[x], logits=False) for x in ['extent', 'border', 'distance', 'hsv']]
        loss = sum(losses)
        #loss = self.loss_fn(preds['border'], border)

        # only use the second channel (one-hot encoding) to allow comparing to other models since the other is 1 - p
        pred_extent = (preds['extent'] > 0.5)
        res_extent = self.matrix_dict(pred_extent[:,1].long(), extent[:,1].long())

        pred_border = (preds['border'] > 0.5)
        res_border = self.matrix_dict(pred_border[:,1].long(), border[:,1].long())

        res = {
            'loss': loss,
            'extent': res_extent,
            'border': res_border,
        }
        self.outputs[stage].append(res)
        return loss
    
    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        dataset_iou_extent, f1_score_extent = self.compute_metrics(outputs, 'extent')
        dataset_iou_border, f1_score_border = self.compute_metrics(outputs, 'border')

        # aggregate lossf
        loss = torch.mean(torch.cat([x['loss'].reshape(1) for x in outputs]))

        metrics_pbar = {
            "loss": loss,
            f"{stage}_extent_iou": dataset_iou_extent,
            f"{stage}_border_iou": dataset_iou_border,
        }

        metrics_other = {
            f"{stage}_extent_f1_score": f1_score_extent,
            f"{stage}_border_f1_score": f1_score_border
        }

        self.log_metrics(metrics_pbar, metrics_other, stage)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()



class LightningModel2(pl.LightningModule):

    def __init__(self, model, loss, lr, **kwargs):
        super().__init__()
        self.model = model

        self.save_hyperparameters("lr")

        self.outputs = {'train':[], 'valid':[], 'test':[]}

        self.loss_fn = loss

    def forward(self, image):
        mask = self.model(image)
        return mask

    def shared_step(self, batch, stage):

        image = batch["image"]

        # Shape of the image should be (batch_size, num_channels, height, width)
        # if you work with grayscale images, expand channels dim to have [batch_size, 1, height, width]
        assert image.ndim == 4

        # Check that image dimensions are divisible by 32,
        # encoder and decoder connected by `skip connections` and usually encoder have 5 stages of
        # downsampling by factor 2 (2 ^ 5 = 32); e.g. if we have image with shape 65x65 we will have
        # following shapes of features in encoder and decoder: 84, 42, 21, 10, 5 -> 5, 10, 20, 40, 80
        # and we will get an error trying to concat these features
        h, w = image.shape[2:]
        assert h % 32 == 0 and w % 32 == 0

        extent = batch["extent"]

        # Shape of the mask should be [batch_size, num_classes, height, width]
        # for binary segmentation num_classes = 1
        assert extent.ndim == 4

        # Check that mask values in between 0 and 1, NOT 0 and 255 for binary segmentation
        assert extent.max() <= 1.0 and extent.min() >= 0

        preds = self.forward(image)

        # Predicted mask contains logits, so loss_fn param `from_logits` is set to True
        losses = [self.loss_fn(preds[x], batch[x], logits=False) for x in ['extent', 'border', 'distance', 'hsv']]
        loss = sum(losses)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        pred_extent = preds['extent'].argmax(dim=1).unsqueeze(1)

        
        tp_extent, fp_extent, fn_extent, tn_extent = smp.metrics.get_stats(pred_extent.long(), extent.long()[:,[0]], mode="binary")


        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        pred_border = preds['border'].argmax(dim=1).unsqueeze(1)

        tp_border, tp_border, fn_border, tn_border = smp.metrics.get_stats(pred_border.long(), batch['border'].long()[:,[0]], mode="binary")

        res = {
            'loss': loss,
            'extent':{
            "tp": tp_extent,
            "fp": fp_extent,
            "fn": fn_extent,
            "tn": tn_extent},
            'border':{
            "tp": tp_border,
            "fp": tp_border,
            "fn": fn_border,
            "tn": tn_border
            }
        }
        self.outputs[stage].append(res)
        return loss
    

    def compute_metrics(self, outputs, key):
        tp = torch.cat([x[key]["tp"] for x in outputs])
        fp = torch.cat([x[key]["fp"] for x in outputs])
        fn = torch.cat([x[key]["fn"] for x in outputs])
        tn = torch.cat([x[key]["tn"] for x in outputs])

        dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
    
        f1_score = tp.sum()/(tp.sum() + 0.5*(fp.sum() + fn.sum()))
        return dataset_iou, f1_score


    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]

        dataset_iou_extent, f1_score_extent = self.compute_metrics(outputs, 'extent')
        dataset_iou_border, f1_score_border = self.compute_metrics(outputs, 'border')

        # aggregate lossf
        loss = torch.cat([x['loss'].reshape(1) for x in outputs])

        metrics = {
            f"{stage}_extent_iou": dataset_iou_extent,
            f"{stage}_extent_f1_score": f1_score_extent,
            f"{stage}_border_iou": dataset_iou_border,
            f"{stage}_border_f1_score": f1_score_border
        }

        self.log_dict(metrics, prog_bar=True)

        self.log_dict({'loss':torch.mean(loss)}, prog_bar=True)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()

    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train")

    def on_train_epoch_end(self):
        return self.shared_epoch_end("train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid")

    def on_validation_epoch_end(self):
        return self.shared_epoch_end("valid")

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test")

    def on_test_epoch_end(self):
        return self.shared_epoch_end("test")
