import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp

from .lightning_model import BaseLightingModel


class LightningModel(BaseLightingModel):

    def shared_step(self, batch, stage):
        image = batch["image"]
        mask = batch["mask"]
        self.assertions(image, mask)

        logits_mask = self.forward(image)

        loss = self.loss_fn(logits_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = prob_mask > 0.5

        res = self.matrix_dict(pred_mask.long(), mask.long())
        res["loss"] = loss

        self.outputs[stage].append(res)
        return res

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]
        dataset_iou, f1_score = self.compute_metrics(outputs)

        # aggregate lossf
        loss = torch.cat([x["loss"].reshape(1) for x in outputs]).mean()

        # these metrics are meant to be shown, but only for validate
        metrics_pbar = {
            "loss": loss,
            f"{stage}_iou": dataset_iou,
        }

        # these metrics will never be shown
        metrics_other = {f"{stage}_f1_score": f1_score}

        self.log_metrics(metrics_pbar, metrics_other, stage)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()
