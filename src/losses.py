"""Calculating the loss
You can build the loss function of BsiNet by combining multiple losses
"""

import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F


def dice_loss(prediction, target):
    """Calculating the dice loss
    Args:
        prediction = predicted image
        target = Targeted image
    Output:
        dice_loss"""

    smooth = 1.0

    i_flat = prediction.view(-1)
    t_flat = target.view(-1)

    intersection = (i_flat * t_flat).sum()

    return 1 - ((2.0 * intersection + smooth) / (i_flat.sum() + t_flat.sum() + smooth))


def calc_loss(prediction, target, bce_weight=0.5):
    """Calculating the loss and metrics
    Args:
        prediction = predicted image
        target = Targeted image
        metrics = Metrics printed
        bce_weight = 0.5 (default)
    Output:
        loss : dice loss of the epoch"""
    bce = F.binary_cross_entropy_with_logits(prediction, target)
    prediction = torch.sigmoid(prediction)
    dice = dice_loss(prediction, target)

    loss = bce * bce_weight + dice * (1 - bce_weight)

    return loss


class log_cosh_dice_loss(nn.Module):
    def __init__(self, num_classes=1, smooth=1, alpha=0.7):
        super(log_cosh_dice_loss, self).__init__()
        self.smooth = smooth
        self.alpha = alpha
        self.num_classes = num_classes

    def forward(self, outputs, targets):
        x = self.dice_loss(outputs, targets)
        return torch.log((torch.exp(x) + torch.exp(-x)) / 2.0)

    def dice_loss(self, y_pred, y_true):
        """[function to compute dice loss]
        Args:
            y_true ([float32]): [ground truth image]
            y_pred ([float32]): [predicted image]
        Returns:
            [float32]: [loss value]
        """
        smooth = 1.0
        y_true = torch.flatten(y_true)
        y_pred = torch.flatten(y_pred)
        intersection = torch.sum((y_true * y_pred))
        coeff = (2.0 * intersection + smooth) / (
            torch.sum(y_true) + torch.sum(y_pred) + smooth
        )
        return 1.0 - coeff


def focal_loss(predict, label, alpha=0.6, beta=2):
    probs = torch.sigmoid(predict)
    # 交叉熵Loss
    ce_loss = nn.BCELoss()
    ce_loss = ce_loss(probs, label)
    alpha_ = torch.ones_like(predict) * alpha
    # 正label 为alpha, 负label为1-alpha
    alpha_ = torch.where(label > 0, alpha_, 1.0 - alpha_)
    probs_ = torch.where(label > 0, probs, 1.0 - probs)
    # loss weight matrix
    loss_matrix = alpha_ * torch.pow((1.0 - probs_), beta)
    # 最终loss 矩阵，为对应的权重与loss值相乘，控制预测越不准的产生更大的loss
    loss = loss_matrix * ce_loss
    loss = torch.sum(loss)
    return loss


class Loss:
    def __init__(self, dice_weight=0.0, class_weights=None, num_classes=1, device=None):
        self.device = device
        if class_weights is not None:
            nll_weight = torch.from_numpy(class_weights.astype(np.float32)).to(
                self.device
            )
        else:
            nll_weight = None
        self.nll_loss = nn.NLLLoss2d(weight=nll_weight)
        self.dice_weight = dice_weight
        self.num_classes = num_classes

    def __call__(self, outputs, targets):
        loss = self.nll_loss(outputs, targets)
        if self.dice_weight:
            eps = 1e-7
            cls_weight = self.dice_weight / self.num_classes
            for cls in range(self.num_classes):
                dice_target = (targets == cls).float()
                dice_output = outputs[:, cls].exp()
                intersection = (dice_output * dice_target).sum()
                # union without intersection
                uwi = dice_output.sum() + dice_target.sum() + eps
                loss += (1 - intersection / uwi) * cls_weight
            loss /= 1 + self.dice_weight
        return loss


class LossMulti:
    def __init__(
        self, jaccard_weight=0.0, class_weights=None, num_classes=1, device=None
    ):
        self.device = device
        if class_weights is not None:
            nll_weight = torch.from_numpy(class_weights.astype(np.float32)).to(
                self.device
            )
        else:
            nll_weight = None

        self.nll_loss = nn.NLLLoss(weight=nll_weight)
        self.jaccard_weight = jaccard_weight
        self.num_classes = num_classes

    def __call__(self, outputs, targets):

        targets = targets.squeeze(1)

        loss = (1 - self.jaccard_weight) * self.nll_loss(outputs, targets)

        if self.jaccard_weight:
            eps = 1e-7  # 原先是1e-7
            for cls in range(self.num_classes):
                jaccard_target = (targets == cls).float()
                jaccard_output = outputs[:, cls].exp()
                intersection = (jaccard_output * jaccard_target).sum()

                union = jaccard_output.sum() + jaccard_target.sum()
                loss -= (
                    torch.log((intersection + eps) / (union - intersection + eps))
                    * self.jaccard_weight
                )
        return loss


class LossBsiNet:
    def __init__(self, weights=[1, 1, 1], num_classes=2):
        self.criterion1 = LossMulti(num_classes=num_classes)  # mask_loss
        self.criterion2 = LossMulti(num_classes=num_classes)  # contour_loss
        self.criterion3 = nn.MSELoss()  ##distance_loss
        self.weights = weights

    def __call__(self, outputs1, outputs2, outputs3, targets1, targets2, targets3):
        #
        criterion = (
            self.weights[0] * self.criterion1(outputs1, targets1)
            + self.weights[1] * self.criterion2(outputs2, targets2)
            + self.weights[2] * self.criterion3(outputs3, targets3)
        )
        return criterion


class LossBsiNetCE:
    def __init__(self, weights=[1, 1, 1], num_classes=2):
        self.criterion1 = nn.CrossEntropyLoss(reduction="mean")  # mask_loss
        self.criterion2 = nn.CrossEntropyLoss(reduction="mean")  # contour_loss
        self.criterion3 = nn.MSELoss()  ##distance_loss
        self.weights = weights

    def __call__(self, outputs1, outputs2, outputs3, targets1, targets2, targets3):
        #
        criterion = (
            self.weights[0] * self.criterion1(outputs1, targets1.squeeze())
            + self.weights[1] * self.criterion2(outputs2, targets2.squeeze())
            + self.weights[2] * self.criterion3(outputs3, targets3)
        )
        return criterion


class LossBsiNetDice:
    def __init__(self, weights=[1, 1, 1]):
        import segmentation_models_pytorch as smp

        self.criterion1 = smp.losses.DiceLoss(mode="multiclass")  # mask_loss
        self.criterion2 = smp.losses.DiceLoss(mode="multiclass")  # contour_loss
        self.criterion3 = nn.MSELoss()  ##distance_loss
        self.weights = weights

    def __call__(self, outputs1, outputs2, outputs3, targets1, targets2, targets3):
        #
        criterion = (
            self.weights[0] * self.criterion1(outputs1, targets1)
            + self.weights[1] * self.criterion2(outputs2, targets2)
            + self.weights[2] * self.criterion3(outputs3, targets3)
        )

        return criterion


def tanimoto_coeff(pred, label):
    smooth = 1e-5

    # class weights
    wli = (label.sum(axis=(2, 3)).mean(axis=0, dtype=torch.float32) ** 2).reciprocal()

    # turn inf elements to zero, then replace with the maximum weight value
    new_weights = torch.where(wli.isinf(), torch.zeros_like(wli), wli)
    wli = torch.where(wli.isinf(), torch.ones_like(wli) * new_weights.max(), wli)

    square_pred = pred**2
    square_target = label**2
    sum_square = (square_pred + square_target).sum(axis=(2, 3))

    sum_product = (pred * label).sum(axis=(2, 3))

    sum_product_labels = (wli * sum_product).sum()

    denominator = sum_square - sum_product

    denominator_sum_labels = (wli * denominator).sum()

    coeff = (sum_product_labels + smooth) / (denominator_sum_labels + smooth)

    return coeff


def tanimoto_loss(pred, label, logits=True):
    if logits:
        pred = torch.sigmoid(pred)
    loss1 = tanimoto_coeff(pred, label)
    loss2 = tanimoto_coeff(1 - pred, 1 - label)
    loss = 1 - 0.5 * (loss1 + loss2)
    return loss


# PyTorch
class DiceLoss(nn.Module):
    def forward(self, inputs, targets, smooth=1e-7, logits=True):
        # we need probabilities so if logits were provided, apply sigmoid
        if logits:
            inputs = F.sigmoid(inputs)

        # flatten label and prediction tensors
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        intersection = (inputs * targets).sum()

        dice = (2.0 * intersection + smooth) / (inputs.sum() + targets.sum() + smooth)

        return 1 - dice


def focal_loss(inputs, targets, gamma=2):
    bce_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction="none")
    pt = torch.exp(-bce_loss)  # prevents nans when probability 0
    loss = (1 - pt) ** gamma * bce_loss
    return loss.mean()


class SAMLoss(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.dice = DiceLoss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, mask_target, iou_input, iou_target):
        loss_focal = self.focal(mask_input, mask_target)
        loss_dice = self.dice(mask_input, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + loss_dice + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_dice}, iou {loss_iou}"
            )

        return loss


class SAMCRFLoss(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.dice = DiceLoss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, crf_potentials, mask_target, iou_input, iou_target):
        loss_focal = self.focal(mask_input, mask_target)
        loss_dice = self.dice(mask_input, mask_target)
        loss_crf = self.dice(crf_potentials, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + (loss_dice + loss_crf) / 2 + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_dice}, crf {loss_crf}, iou {loss_iou}"
            )

        return loss


class UnetCRFLoss(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.dice = DiceLoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, crf_potentials, mask_target):
        loss_dice = self.dice(mask_input, mask_target)
        loss_crf = self.dice(crf_potentials, mask_target)

        loss = (loss_dice + loss_crf) / 2

        if self.checkNaN and torch.isnan(loss):
            raise ValueError(
                f"Detected NaN loss. Loss components are : dice {loss_dice}, crf {loss_crf},"
            )

        return {"loss": loss, "dice": loss_dice, "crf": loss_crf}


def CrossEntropy2d(input, target, weight=None, reduction="mean"):
    """2D version of the cross entropy loss"""
    dim = input.dim()
    if dim == 2:
        return F.cross_entropy(input, target, weight, reduction)
    elif dim == 4:
        output = input.reshape(input.size(0), input.size(1), -1)
        output = torch.transpose(output, 1, 2).contiguous()
        output = output.view(-1, output.size(2))
        target = target.reshape(-1)
        return F.cross_entropy(output, target, weight, reduction)
    else:
        raise ValueError("Expected 2 or 4 dimensions (got {})".format(dim))


class SAMCRFLoss(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.dice = nn.BCELoss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, crf_potentials, mask_target, iou_input, iou_target):
        mask_input = F.sigmoid(mask_input)
        crf_potentials = F.sigmoid(crf_potentials)

        loss_focal = self.focal(mask_input, mask_target)
        loss_dice = self.dice(mask_input, mask_target)
        loss_crf = self.dice(crf_potentials, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + (loss_dice + loss_crf) / 2 + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_dice}, crf {loss_crf}, iou {loss_iou}"
            )

        return {
            "loss": loss,
            "focal": loss_focal,
            "dice": loss_dice,
            "crf": loss_crf,
            "iou": loss_iou,
        }


class SAMCRFLoss01(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.dice = nn.BCELoss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, crf_potentials, mask_target, iou_input, iou_target):
        mask_input = F.sigmoid(mask_input)
        crf_potentials = F.sigmoid(crf_potentials)

        loss_focal = self.focal(mask_input, mask_target)
        loss_dice = self.dice(mask_input, mask_target)
        loss_crf = self.dice(crf_potentials, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + (loss_dice + 0.0 * loss_crf) / 2 + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_dice}, crf {loss_crf}, iou {loss_iou}"
            )

        return {
            "loss": loss,
            "focal": loss_focal,
            "dice": loss_dice,
            "crf": loss_crf,
            "iou": loss_iou,
        }


class SAMCRFLossDice(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.dice = DiceLoss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, crf_potentials, mask_target, iou_input, iou_target):

        loss_focal = self.focal(mask_input, mask_target)
        loss_dice = self.dice(mask_input, mask_target)
        loss_crf = self.dice(crf_potentials, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + (loss_dice + loss_crf) / 2 + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_dice}, crf {loss_crf}, iou {loss_iou}"
            )

        return loss


class SAMLossDice(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.dice = DiceLoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, mask_target, iou_input, iou_target):
        loss_dice = self.dice(mask_input, mask_target)

        loss = loss_dice

        if self.checkNaN and torch.isnan(loss):
            raise ValueError(
                f"Detected NaN loss. Loss components are : dice {loss_dice}"
            )

        return loss


class _SAMLossDistance(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.focal = focal_loss
        self.l1 = nn.L1Loss()
        self.iou_loss = nn.MSELoss()
        self.checkNaN = checkNaN

    def forward(self, mask_input, mask_target, iou_input, iou_target):
        loss_focal = self.focal(mask_input, mask_target)
        loss_l1 = self.l1(mask_input, mask_target)
        loss_iou = self.iou_loss(iou_input, iou_target)

        loss = 20 * loss_focal + loss_l1 + loss_iou

        if self.checkNaN and torch.isnan(loss):
            print(iou_input, iou_target)
            raise ValueError(
                f"Detected NaN loss. Loss components are : focal {loss_focal}, dice {loss_l1}, iou {loss_iou}"
            )

        return loss

    def single_loss(self, mask_input, mask_target):
        loss = self.l1(mask_input, mask_target)
        return loss


class SAMLossDistance(nn.Module):
    def __init__(self, checkNaN=False):
        super().__init__()
        self.l1 = nn.L1Loss()

    def forward(self, mask_output, mask_target):
        loss_l1 = self.l1(mask_output, mask_target)
        return loss_l1
