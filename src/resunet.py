# -*- coding: utf-8 -*-
""" Acknowlegement 
Credits to (1) https://github.com/feevos/resuneta, (2) https://github.com/Akhilesh64/ResUnet-a 
for helping me have a clearer understanding on the ResUNet-a D6 structure.
"""
"""
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import Adam
"""

import torch
import torch.nn as nn

import argparse
import os
import re
import time



HEIGHT = 256
WIDTH = 256
DEPTH = 4
NUM_CLASSES = 2


#BATCH_NORM_ARGS = {'eps':0.001, 'momentum':0.01}
BATCH_NORM_ARGS = {}


class PSPPooling(nn.Module):
    def __init__(self, filters):
        super().__init__()
        seqs = nn.ModuleList([nn.Identity()])
        for i in [1,2,4,8]:
            seq = nn.Sequential(
                nn.MaxPool2d(kernel_size=i, stride=i),
                nn.UpsamplingNearest2d(scale_factor=i),
                nn.LazyConv2d(filters//4, 1),
                nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
                )
            seqs.append(seq)
        self.seqs = seqs
        self.conv = nn.LazyConv2d(filters, 1)
        self.norm = nn.LazyBatchNorm2d(**BATCH_NORM_ARGS)

    def forward(self, x):
        # original contains an error which doesn't concatenate the last sequence in self.seqs
        x = torch.cat([seq(x) for seq in self.seqs[:-1]], dim=1)
       # x = torch.cat([seq(x) for seq in self.seqs], dim=1)
        x = self.conv(x)
        x = self.norm(x)
        return x


class CombineModule(nn.Module):
    def __init__(self, filters):
        super().__init__()
        self.upsampling = nn.UpsamplingNearest2d(scale_factor=2)
        self.activation = nn.ReLU()

        self.conv = nn.LazyConv2d(filters, 1)
        self.norm = nn.LazyBatchNorm2d(**BATCH_NORM_ARGS)
    
    def forward(self, x, y):
        x = self.upsampling(x)
        x = self.activation(x)
        x = torch.cat([x,y], 1)
        x = self.conv(x)
        x = self.norm(x)
        return x


class ResidualBlock(nn.Module):
    def __init__(self, filters, kernel_size, dilation_rates, strides=1):
        super().__init__()   
        self.seqs = nn.ModuleList([nn.Identity()])
        for rate in dilation_rates:
            seq = nn.Sequential(
                nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
                nn.ReLU(),
                nn.LazyConv2d(filters, kernel_size, strides, 'same', rate),
                nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
                nn.ReLU(),
                nn.LazyConv2d(filters, kernel_size, strides, 'same', rate),
            )
            self.seqs.append(seq)
    
    def forward(self, x):
        return sum(seq(x) for seq in self.seqs)
    

# # ----- Define ResUnet-a D6 structure -----
class ResUnet(nn.Module):


    def __init__(self, num_classes=2, input_shape=(4, 256,256)):
        super().__init__()

        self.num_classes = num_classes
        self.channels, self.height, self.width = input_shape
        
        # Encoder

        self.seq1 = nn.Sequential(
            nn.LazyConv2d(32,1, padding='same'),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU()
        )

        self.res1 = ResidualBlock(32, 3, [1,3,15,31])

        self.conv2 = nn.LazyConv2d(64,1, stride=2)
        self.res2 = ResidualBlock(64, 3, [1,3,15,31])

        self.conv3 = nn.LazyConv2d(128, 1, stride=2)
        self.res3 = ResidualBlock(128, 3, [1,3,15])

        self.conv4 = nn.LazyConv2d(256, 1, stride=2)
        self.res4 = ResidualBlock(256, 3, [1,3,15])

        self.conv5 = nn.LazyConv2d(512, 1, stride=2)
        self.res5 = ResidualBlock(512, 3, [1])

        self.conv6 = nn.LazyConv2d(1024, 1, stride=2)
        self.res6 = ResidualBlock(1024, 3, [1])


        # Bridge
        self.bridge = nn.Sequential(
            PSPPooling(1024),
            nn.ReLU()
        )

        # Decoder
        self.combine1 = CombineModule(512)
        self.res7 = ResidualBlock(512, 3, [1,3,15])

        self.combine2 = CombineModule(256)
        self.res8 = ResidualBlock(256, 3, [1,3,15])

        self.combine3 = CombineModule(128)
        self.res9 = ResidualBlock(128, 3, [1,3,15,31])

        self.combine4 = CombineModule(64)
        self.res10 = ResidualBlock(64, 3, [1,3,15,31])

        self.combine5 = CombineModule(32)
        self.res11 = ResidualBlock(32, 3, [1,3,15,31])

        self.combine6 = CombineModule(32)

        self.psppooling = PSPPooling(32)
        
        self.relu = nn.ReLU()

        #Multi-tasking
        #Color (HSV)
        self.hsv = nn.Sequential(
            nn.LazyConv2d(3,1),
            nn.Sigmoid()
        )

        # Distance
        activation = nn.Sigmoid if self.num_classes == 1 else nn.Softmax2d
        self.distance = nn.Sequential(
            nn.ConstantPad2d(1,0),
            nn.LazyConv2d(32,3),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU(),
            nn.ConstantPad2d(1,0),
            nn.LazyConv2d(32,3),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU(),
            nn.LazyConv2d(self.num_classes, 1),
            activation(),
        )

        # Boundaries
        self.boundaries = nn.Sequential(
            nn.ConstantPad2d(1,0),
            nn.LazyConv2d(32,3),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU(),
            nn.LazyConv2d(self.num_classes, 1),
            nn.Sigmoid(),
        )

        # Extents
        self.segmentation = nn.Sequential(
            nn.ConstantPad2d(1,0),
            nn.LazyConv2d(32,3),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU(),
            nn.ConstantPad2d(1,0),
            nn.LazyConv2d(32,3),
            nn.LazyBatchNorm2d(**BATCH_NORM_ARGS),
            nn.ReLU(),
            nn.LazyConv2d(self.num_classes, 1),
            activation(),
        )

    def forward(self, x):
        x = self.seq1(x)            #conv1
        a = x                       

        x = self.res1(x)
        b = x                       #Dn1

        x = self.conv2(x)
        x = self.res2(x)
        c = x                       #Dn2

        x = self.conv3(x)
        x = self.res3(x)
        d = x                       #Dn3

        x = self.conv4(x)
        x = self.res4(x)            
        e = x                       #Dn4

        x = self.conv5(x)
        x = self.res5(x)            
        f = x                       #Dn5

        x = self.conv6(x)
        x = self.res6(x)
        g = x                       #Dn6

        # Bridge
        x = self.bridge(x)

        # Decoder
        x = self.combine1(x, f)     #midddle+Dn5
        x = self.res7(x)

        x = self.combine2(x, e)      #UpConv1+Dn4
        x = self.res8(x)

        x = self.combine3(x, d)      #UpConv2+Dn3
        x = self.res9(x)

        x = self.combine4(x, c)      #UpConv3+Dn2
        x = self.res10(x)

        x = self.combine5(x, b)      #UpConv4+Dn1
        x = self.res11(x)

        #x1 = self.combine6(x, a)
        x1 = torch.cat([x, a], 1)    #UpConv5+conv1
        x = self.psppooling(x1)
        x = self.relu(x)

        # Multi-tasking
        # Color (HSV)
        hsv = self.hsv(x1)

        # Distance
        distance = self.distance(x1)

        # Boundaries
        boundaries = torch.cat([x, distance], 1)
        boundaries = self.boundaries(boundaries)

        # Extents
        segmentation = torch.cat([x, boundaries, distance], 1)
        segmentation = self.segmentation(segmentation)

        return {'extent': segmentation, 
                'border': boundaries, 
                'distance': distance,
                'hsv': hsv}