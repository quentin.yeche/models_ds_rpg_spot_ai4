import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp
from src.tools import move_dic

from .lightning_model import BaseLightingModel


class LightningModel(BaseLightingModel):

    def shared_step(self, batch, stage):
        image = batch["image"]
        mask = batch["mask"]
        self.assertions(image, mask)

        pred = self.forward(image)
        logits_mask = pred["extent"]

        crf_potentials = pred["crf_potentials"]

        loss_dic = self.loss_fn(logits_mask, crf_potentials, mask)

        loss = loss_dic["loss"]

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then
        # apply thresholding
        prob_mask = logits_mask.sigmoid()
        pred_mask = prob_mask > 0.5

        res = self.matrix_dict(pred_mask.long(), mask.long())
        res = res | move_dic(loss_dic, "cpu")
        self.outputs[stage].append(res)
        return loss

    def shared_epoch_end(self, stage):
        outputs = self.outputs[stage]
        iou, f1 = self.compute_metrics(outputs)

        # aggregate lossf
        loss = torch.cat([x["loss"].reshape(1) for x in outputs]).mean()
        dice = torch.cat([x["dice"].reshape(1) for x in outputs]).mean()
        crf = torch.cat([x["crf"].reshape(1) for x in outputs]).mean()

        # these metrics are meant to be shown, but only for validate
        metrics_pbar = {
            f"{stage}_iou": iou,
            f"{stage}_loss": loss,
            f"{stage}_dice": dice,
            f"{stage}_crf": crf,
        }

        # these metrics will never be shown
        metrics_other = {
            f"{stage}_f1": f1,
        }

        self.log_metrics(metrics_pbar, metrics_other, stage)

        # clear the list of outputs to reset for the next epoch
        outputs.clear()
