from torch.utils.data import Dataset as BaseDataset
import torch
import rasterio as rio
from pathlib import Path
import numpy as np
from skimage.color import rgb2hsv
import warnings
from multiprocessing import Pool
from tqdm.auto import tqdm
from itertools import repeat


def read_image(fp):
    return rio.open(fp).read()


def read_band(fp, band, band_type):
    ar = rio.open(fp, dtype=band_type).read()[band]
    return ar


class MaskDataset(BaseDataset):
    """Dataset. Read images, apply augmentation and preprocessing transformations.
    Single mask

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files
        mask_band (int): band number (0-indexed) to read from the mask file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        mask_paths,
        mask_band,
        masks,
    ):

        self.ids = [patch.name for patch in mask_paths]
        self.mask_paths = mask_paths

        self.mask_band = mask_band
        self.augmentation = None
        self.masks = masks

    def __getitem__(self, i):
        mask = self.masks[i]
        id = self.ids[i]

        return {"mask": mask[None, ...], "id": id}

    def __len__(self):
        return len(self.ids)


class Dataset(BaseDataset):
    """Dataset. Read images, apply augmentation and preprocessing transformations.
    Single mask

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files
        mask_band (int): band number (0-indexed) to read from the mask file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths,
        mask_paths,
        mask_band,
        augmentation=None,
        normalize=None,
        mask_type=None,
    ):
        match mask_type:
            case None:
                mask_type = "int16"
                torch_type = torch.int16
            case "float32":
                torch_type = torch.float32

        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths
        self.mask_paths = mask_paths

        self.mask_band = mask_band
        self.mask_type = mask_type
        self.augmentation = augmentation
        self.normalize = {key: np.array(val) for key, val in normalize.items()}

        pool = Pool(8)

        with rio.open(self.image_paths[0]) as ds:
            height = ds.meta["height"]
            width = ds.meta["width"]
            count = ds.meta["count"]

        self.images = torch.zeros(
            (len(self.image_paths), count, width, height), dtype=torch.int32
        )
        for i, res in tqdm(
            enumerate(pool.map(read_image, self.image_paths)),
            total=len(self),
            desc="Reading images",
        ):
            self.images[i] = torch.from_numpy(res.astype(np.int32))

        self.masks = torch.zeros(
            (len(self.mask_paths), 1, width, height), dtype=torch_type
        )
        for i, res in enumerate(
            pool.starmap(
                read_band,
                tqdm(
                    zip(
                        self.mask_paths, repeat(self.mask_band), repeat(self.mask_type)
                    ),
                    total=len(self),
                    desc="Reading masks",
                ),
            )
        ):
            self.masks[i] = torch.from_numpy(res.astype(self.mask_type))

        if self.normalize:
            images = self.images.permute(0, 2, 3, 1)
            images = (images - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            self.images = images.permute(0, 3, 1, 2).float()

    def __getitem__(self, i):
        image = self.images[i]
        mask = self.masks[i]
        id = self.ids[i]

        # apply augmentations
        if self.augmentation:
            image, mask = self.augmentation(image, mask)

        # TODO remove [None,...] and add an usqueeze() to self.masks
        return {"image": image, "mask": mask, "id": id}

    def __len__(self):
        return len(self.ids)


class SamDataset(Dataset):
    def __init__(
        self,
        image_paths,
        mask_paths,
        mask_band,
        prefix,
        augmentation=None,
        normalize=None,
    ):
        super().__init__(image_paths, mask_paths, mask_band, augmentation, normalize)
        self.prefix = prefix

    def __getitem__(self, i):
        batch = super().__getitem__(i)
        image = batch["image"][:3].repeat_interleave(4, 1).repeat_interleave(4, 2)
        res = {f"{self.prefix}_image": image, f"{self.prefix}_label": batch["mask"]}
        return res


class MobileSamDataset(Dataset):
    def __getitem__(self, i):
        batch = super().__getitem__(i)
        image = torch.nn.functional.interpolate(
            batch["image"][:3].unsqueeze(0),
            1024,
            mode="bilinear",
            align_corners=False,
            antialias=True,
        ).squeeze()
        # consider putting .to(device) somewhere else (lightning forward?) or adding a way to change it
        res = {
            "image": image,
            "mask": batch["mask"],
            "original_size": (256, 256),
            "id": batch["id"],
        }
        return res


class CompositeMobileDataset(Dataset):
    def __getitem__(self, i):
        batch = super().__getitem__(i)
        image = torch.nn.functional.interpolate(
            batch["image"][[3, 0, 1]].unsqueeze(0),
            1024,
            mode="bilinear",
            align_corners=False,
            antialias=True,
        ).squeeze()
        # consider putting .to(device) somewhere else (lightning forward?) or adding a way to change it
        res = {
            "image": image,
            "mask": batch["mask"],
            "original_size": (256, 256),
            "id": batch["id"],
        }
        return res


class AdaptedMobileSamDataset(Dataset):
    def __getitem__(self, i):
        batch = super().__getitem__(i)
        image = torch.nn.functional.interpolate(
            batch["image"].unsqueeze(0),
            1024,
            mode="bilinear",
            align_corners=False,
            antialias=True,
        ).squeeze()
        # consider putting .to(device) somewhere else (lightning forward?) or adding a way to change it
        res = {
            "image": image,
            "mask": batch["mask"],
            "original_size": (256, 256),
            "id": batch["id"],
        }
        return res


"""
        Arguments:
          batched_input (list(dict)): A list over input images, each a
            dictionary with the following keys. A prompt key can be
            excluded if it is not present.
              'image': The image as a torch tensor in 3xHxW format,
                already transformed for input to the model.
              'original_size': (tuple(int, int)) The original size of
                the image before transformation, as (H, W).
              'point_coords': (torch.Tensor) Batched point prompts for
                this image, with shape BxNx2. Already transformed to the
                input frame of the model.
              'point_labels': (torch.Tensor) Batched labels for point prompts,
                with shape BxN.
              'boxes': (torch.Tensor) Batched box inputs, with shape Bx4.
                Already transformed to the input frame of the model.
              'mask_inputs': (torch.Tensor) Batched mask inputs to the model,
                in the form Bx1xHxW.
          multimask_output (bool): Whether the model should predict multiple
            disambiguating masks, or return a single mask.

"""








class TriDataset(BaseDataset):
    """TriDataset. Read images, apply augmentation and preprocessing transformations.
    Three masks: extent, border, distance

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files (extent and border)
        distance_paths (str or Path list): paths to the distance files
        extent_band (int): band number (0-indexed) to read the extent from the mask file
        border_band (int): band number (0-indexed) to read the border from the mask file
        distance_band (int): band number (0-indexed) to read the distance from the distance file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths,
        mask_paths,
        distance_paths,
        extent_band,
        border_band,
        distance_band,
        augmentation=None,
        normalize=None,
    ):
        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths
        self.mask_paths = mask_paths
        self.distance_paths = distance_paths

        self.extent_band = extent_band
        self.border_band = border_band
        self.distance_band = distance_band

        self.augmentation = augmentation
        self.normalize = {key: np.array(val) for key, val in normalize.items()}


        with rio.open(self.image_paths[0]) as ds:
            height = ds.meta["height"]
            width = ds.meta["width"]
            count = ds.meta["count"]

        pool = Pool(8)

        self.images = torch.zeros(
            (len(self.image_paths), count, width, height), dtype=torch.int32
        )
        for i, res in tqdm(
            enumerate(pool.map(read_image, self.image_paths)),
            total=len(self),
            desc="Reading images",
        ):
            self.images[i] = torch.from_numpy(res.astype(np.int32))


        self.extents = torch.zeros(
            (len(self.mask_paths), 1, width, height), dtype=torch.int32
        )

        self.borders = torch.zeros(
            (len(self.mask_paths), 1, width, height), dtype=torch.int32
        )

        for i, res in tqdm(
            enumerate(pool.map(read_image, self.mask_paths)),
            total=len(self),
            desc="Reading masks",
        ):
            self.extents[i] = torch.from_numpy(res[self.extent_band])
            self.borders[i] = torch.from_numpy(res[self.border_band])

        names_distances = [path.name for path in self.distance_paths]
        names_images = [path.name for path in self.image_paths]
        assert names_distances == names_images
        

        self.distances = torch.zeros(
            (len(self.mask_paths), 1, width, height), dtype=torch.float32
        )
        for i, res in tqdm(
            enumerate(pool.map(read_image, self.distance_paths)),
            total=len(self),
            desc="Reading distances",
        ):
            self.distances[i] = torch.from_numpy(res[self.distance_band])

        if self.normalize:
            images = self.images.permute(0, 2, 3, 1)
            images = (images - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            self.images = images.permute(0, 3, 1, 2).float()

    def __getitem__(self, i):
        # read data
        image = self.images[i]
        extent = self.extents[i]
        border = self.borders[i]
        distance = self.distances[i]

        id = self.ids[i]

        # apply augmentations
        if self.augmentation:
            image, extent, border, distance = self.augmentation(
                image, extent, border, distance
            )

        return {
            "image": image,
            "extent": extent.long(),
            "border": border.long(),
            "distance": distance,
            "id": id,
        }

    def __len__(self):
        return len(self.ids)


class TriDataset2(BaseDataset):
    """TriDataset. Read images, apply augmentation and preprocessing transformations.
    Three masks: extent, border, distance

    Args:
        image_paths (str or Path list): paths to the image files
        mask_paths (str or Path list): paths to the mask files (extent and border)
        distance_paths (str or Path list): paths to the distance files
        extent_band (int): band number (0-indexed) to read the extent from the mask file
        border_band (int): band number (0-indexed) to read the border from the mask file
        distance_band (int): band number (0-indexed) to read the distance from the distance file
        augmentation (nn.Module): data augmentation to apply
            (e.g. flip, scale, etc.)
        normalize (dict): min/max values for normalization

    """

    def __init__(
        self,
        image_paths,
        mask_paths,
        distance_paths,
        extent_band,
        border_band,
        distance_band,
        augmentation=None,
        normalize=None,
    ):
        self.ids = [patch.name for patch in image_paths]
        self.image_paths = image_paths
        self.mask_paths = mask_paths
        self.distance_paths = distance_paths

        self.extent_band = extent_band
        self.border_band = border_band
        self.distance_band = distance_band

        self.augmentation = augmentation
        self.normalize = {key: np.array(val) for key, val in normalize.items()}

        self.images = torch.from_numpy(
            np.array([rio.open(fp).read() for fp in self.image_paths]).astype(np.int32)
        )
        extents, borders = [], []
        for fp in self.mask_paths:
            with rio.open(fp) as ds:
                ar = ds.read()
                extents.append(ar[self.extent_band])
                borders.append(ar[self.border_band])
        self.extents = torch.from_numpy(np.array(extents))

        # should help with not running out of memory
        del extents
        self.borders = torch.from_numpy(np.array(borders))
        del borders
        names_distances = [path.name for path in self.distance_paths]
        names_images = [path.name for path in self.image_paths]
        assert names_distances == names_images
        self.distances = torch.from_numpy(
            np.array(
                [rio.open(fp).read()[self.distance_band] for fp in self.distance_paths]
            )
        )

        if self.normalize:
            images = self.images.permute(0, 2, 3, 1)
            images = (images - self.normalize["min"]) / (
                self.normalize["max"] - self.normalize["min"]
            )
            self.images = images.permute(0, 3, 1, 2).float()

    def __getitem__(self, i):
        # read data
        image = self.images[i]
        extent = self.extents[i].unsqueeze(0)
        border = self.borders[i].unsqueeze(0)
        distance = self.distances[i].unsqueeze(0)

        id = self.ids[i]

        # apply augmentations
        if self.augmentation:
            image, extent, border, distance = self.augmentation(
                image, extent, border, distance
            )

        return {
            "image": image,
            "extent": extent.long(),
            "border": border.long(),
            "distance": distance,
            "id": id,
        }

    def __len__(self):
        return len(self.ids)


class QuadDataset(TriDataset):
    """Small modification of TriDataset which:
    - produces an HSV version of the image on the fly
    - converts extents to one-hot encoding
    - adds a second (1-p) class to contour and distance
    """

    def __getitem__(self, i):
        dic = super().__getitem__(i)
        image = dic["image"]
        # rgb2hsv tends to produce warnings as part of its normal operation
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            hsv = rgb2hsv(image[:3].numpy(), channel_axis=0)
            hsv = torch.from_numpy(hsv)

        extent = dic["extent"].squeeze()
        extent = torch.eye(2)[extent.type(torch.int32)].permute(2, 0, 1)

        border = dic["border"]
        contour_bis = 1 - border
        border = torch.cat([contour_bis, border], 0)

        distance = dic["distance"]
        distance_bis = 1 - distance
        distance = torch.cat([distance_bis, distance], 0)

        return {
            "image": image.type(torch.float32),
            "extent": extent.type(torch.float32),
            "border": border.type(torch.float32),
            "distance": distance.type(torch.float32),
            "hsv": hsv,
            "id": dic["id"],
        }
