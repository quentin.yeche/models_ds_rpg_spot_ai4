import pytorch_lightning as pl
import torch
import segmentation_models_pytorch as smp

class BaseLightingModel(pl.LightningModule):

    def __init__(self, model, loss, lr):
        super().__init__()
        self.model = model

        self.save_hyperparameters("lr")

        self.outputs = {'train':[], 'valid':[], 'test':[]}

        self.loss_fn = loss

    def forward(self, image):
        mask = self.model(image)
        return mask
    
    def assertions(self, image, mask):
        # Shape of the image should be (batch_size, num_channels, height, width)
        # if you work with grayscale images, expand channels dim to have [batch_size, 1, height, width]
        assert image.ndim == 4

        # Check that image dimensions are divisible by 32,
        # encoder and decoder connected by `skip connections` and usually encoder have 5 stages of
        # downsampling by factor 2 (2 ^ 5 = 32); e.g. if we have image with shape 65x65 we will have
        # following shapes of features in encoder and decoder: 84, 42, 21, 10, 5 -> 5, 10, 20, 40, 80
        # and we will get an error trying to concat these features
        h, w = image.shape[2:]
        assert h % 32 == 0 and w % 32 == 0

        # Shape of the mask should be [batch_size, num_classes, height, width]
        # for binary segmentation num_classes = 1
        assert mask.ndim == 4

        # Check that mask values in between 0 and 1, NOT 0 and 255 for binary segmentation
        assert mask.max() <= 1.0 and mask.min() >= 0

    def matrix_dict(self, pred, gt):
        tp, fp, fn, tn = smp.metrics.get_stats(pred, gt, mode="binary")
        res = { "tp": tp,"fp": fp,
               "fn": fn, "tn": tn}
        return res
    
    def log_metrics(self, metrics_pbar, metrics_other, stage):
        show_pbar = stage == 'valid'
        self.log_dict(metrics_pbar, prog_bar=show_pbar)
        self.log_dict(metrics_other, prog_bar=False)



    #key is not used for single output models
    def compute_metrics(self, outputs, key=None):
        if key is None:
            tp = torch.cat([x["tp"] for x in outputs])
            fp = torch.cat([x["fp"] for x in outputs])
            fn = torch.cat([x["fn"] for x in outputs])
            tn = torch.cat([x["tn"] for x in outputs])
        else:
            tp = torch.cat([x[key]["tp"] for x in outputs])
            fp = torch.cat([x[key]["fp"] for x in outputs])
            fn = torch.cat([x[key]["fn"] for x in outputs])
            tn = torch.cat([x[key]["tn"] for x in outputs])
        dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
    
        f1_score = tp.sum()/(tp.sum() + 0.5*(fp.sum() + fn.sum()))
        return dataset_iou, f1_score


    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train")

    def on_train_epoch_end(self):
        return self.shared_epoch_end("train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid")

    def on_validation_epoch_end(self):
        return self.shared_epoch_end("valid")

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test")

    def on_test_epoch_end(self):
        return self.shared_epoch_end("test")
